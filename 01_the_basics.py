import unittest


class TestPython(unittest.TestCase):
	def test_true(self):
		monkeys_eat_bananas = True
		self.assertTrue(monkeys_eat_bananas)


	def test_false(self):
		roses_are_blue = False
		self.assertFalse(roses_are_blue)


	def test_math_is_fun(self):
		addition_is_hard = (41 + 1)
		answer_to_everything = 42
		self.assertTrue(addition_is_hard == answer_to_everything)


	def test_falseys(self):
		falseys = [0, None, [], (), {}, '']

		for falsey in falseys:
			self.assertFalse(falsey)


	def test_false_equals_false(self):
		self.assertEqual(False, False)


	def test_none_equals_none(self):
		self.assertEqual(None, None)


	def test_none_not_equal_to_zero(self):
		self.assertNotEqual(None, 0)


	def test_len_is_a_function(self):
		test_string = 'test_string'
		self.assertTrue(hasattr(len, '__call__'))
		self.assertEqual(len.__call__(test_string), len(test_string))


	def test_string_length(self):
		long_string = 'this_string_is_pretty_long'
		length_of_string = len(long_string)
		expected_length_of_string = 26

		self.assertEqual(length_of_string, expected_length_of_string)


	def test_array(self):
		array = [1, 2, 3, 7]
		length_of_array = len(array)

		self.assertTrue(length_of_array == 4)
		self.assertTrue(array[3] == 7)


	def test_tuple(self):
		tuple = (1, 2, 3, 7)
		length_of_tuple = len(tuple)

		self.assertTrue(length_of_tuple == 4)
		self.assertTrue(tuple[3] == 7)


	def test_adding_to_tuple(self):
		# with self.assertRaises(Exception) as context:
		tuple = (1, 2, 3)
		tuple += (4, 5)
		length_of_tuple = len(tuple)
		print length_of_tuple
		self.assertTrue(length_of_tuple == 5)


	def test_get_dictionary(self):
		dictionary = {'key': 1}

		self.assertTrue(len(dictionary) == 1)
		self.assertTrue(dictionary['key'] == 1)
		self.assertTrue(dictionary.get('key') == 1)
		self.assertTrue(dictionary.get('fake_key', 99) == 99)


	def test_set_dictionary(self):
		dictionary = {'key': 1}
		dictionary['another_key'] = 2

		self.assertTrue(dictionary.get('another_key') == 2)


	def test_map(self):
		array = [{'key': 1}, {'key': 2}, {'key': 3}]
		mapped_array = map(lambda x: x.get('key'), array)

		self.assertTrue(len(mapped_array) == 3)
		self.assertTrue(mapped_array[0] == 1)


if __name__ == '__main__':
	unittest.main()
